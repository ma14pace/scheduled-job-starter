# Starter project for a Scheduled Job Starter with Docker Smart CI

This project is a [Docker Smart CI](https://stp-test.wien.gv.at/docker-smart-ci/) sample 
project for a job, that is started with a cron-like schedule.

The basic idea is, that the "scheduler" is just bash script, looping and sleeping. If it wakes 
up and finds work, the work is executed synchronously. After execution, the script sleeps
until the next execution begins. The priority is not on accuracy and timeliness. 

The pattern is sufficient for daily or hourly jobs.

As an example, we start a Python script. Therefore, we use a Python base image. For executors
coded in other languages, an appropriate base image would have to be used.

## Creating a new repository from this starter

* Read the docs of [Docker Smart CI](https://stp-test.wien.gv.at/docker-smart-ci/)

* Create the new repository (e.g. `my-new-job`) in Bitbucket. Leave it empty, don't
  add a README. You should see instructions titled _Let's put some bits in your bucket_.
  Keep it open, but don't do it now

* Check out [this starter](https://bitbucket.org/ma14pace/scheduled-job-starter/)
  with `git clone https://bitbucket.org/ma14pace/scheduled-job-starter.git my-new-job`
  under the new name (here: `my-new-job`)

* Change into that new directory

* Remove the old remote: `git remote remove origin`

* Add the new remote: `git remote add origin https://bitbucket.org/ma14pace/my-new-job.git`,
  replacing `my-new-job` with the name of your project

* Add another remote to a development repo (optional)

* Push to the new repository: `git push -u origin master`

## Architecture

There is a single service called _py-jobs_ in a subdirectory [py-jobs](./py-jobs).
It contains a [Dockerfile](./py-jobs/Dockerfile) and a sample [scheduler](./py-jobs/scheduler.sh)
shell script. The Dockerfile derives from [python:3](https://hub.docker.com/_/python), which is 
the generic Python 3, at time of writing this is version 3.9.1.

If no Python is needed for the jobs, a smaller image could be used, but not Alpine, because
we need a current `bash`, and Alpine comes with `ash`.

There is a subdirectory for the jobs under [py-jobs/jobs](./py-jobs/jobs). It contains a directory
each job, and each of those directories must contain a file `job.sh`. This is the structure of 
the project:

```
.
|-- CURRENT_CONFIG_VERSION
|-- README.md
|-- data
|-- docker-compose.yml
|-- logs
`-- py-jobs
    |-- Dockerfile
    |-- jobs
    |   |-- test_daily
    |   |   `-- job.sh
    |   |-- test_every_minute
    |   |   `-- job.sh
    |   `-- test_hourly_py
    |       |-- get_news.py
    |       |-- job.sh
    |       `-- requirements.txt
    |-- proxy.env
    |-- proxy.env.j2
    |-- scheduler.sh
    `-- test-schedule.txt
```

Jobs and schedule are built into the image, but if needed, both could as well be
mapped into the container.

[py-jobs/test-schedule.txt](./py-jobs/test-schedule.txt) is a test schedule, and it is also
the default schedule, if [scheduler](./py-jobs/scheduler.sh) is called without a parameter.

That's what it looks like:

```
# This is a sample schedule
#
# Nothing will be run at 00:00, instead the job-last-run ledger is reset
#

# test will be executed every hour at 49
*:49:test_hourly_py

# test will be executed daily at 14:58 UTC
14:58:test_daily

# test will be executed every minute
*:*:test_every_minute
```

Blank lines and lines starting with `#` are ignored. Please note
that time in the container is UTC.

One of the jobs, `test_hourly_py`, fetches the headline of the current top story
of our city's website.

Two directories are mounted into the container, `data` and `logs`. Normal logging 
is to STDOUT, but the idea is that jobs might produce data, and while doing so, they
might write logs, that we probably want to log per job or per invocation. Skip
these directories, if you don't need them.

`CURRENT_CONFIG_VERSION` is included with the project. In production it is generated
upon deployment, and it contains the semantic version tag.