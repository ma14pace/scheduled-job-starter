#!/usr/bin/env bash

# make sure we are in script directory
cd "$(dirname $0)"

# dictionary/map/hash vars not available before bash 4
if [[ $(echo $BASH_VERSION | cut -d. -f1) -lt 4 ]] ; then
  echo "$0: needs BASH major version >= 4"
  exit 1
fi

# The schedule file with lines in format '*:job1' and/or '13:job2',
# where '*' means "every hour" and "13" means in the hour between
# 13:00 and 13:59:59
# "job1" and "job2" must exist as subdirectories of "jobs" and must
# contain an executable bash script job.sh
#
# The schedule file can be given as a parameter, or if not, a default
# schedule for testing is used. The default schedule executes a test
# job once an hour. The test job just prints the Python version
SCHEDULE_FILE="test-schedule.txt"
if [[ $# -gt 0  && -f "$1" ]] ; then
  SCHEDULE_FILE="$1"
fi

# dictionary for recording last execution time per job; requires bash 4
declare -A LEDGER=()

while true; do

  # take the current time
  THIS_HOUR="$(date +'%H')"
  THIS_MINUTE="$(date +'%M')"
  THIS_TIME="${THIS_HOUR}:${THIS_MINUTE}"

  # reset ledger at midnight
  if [[ ${THIS_TIME} == "00:00" ]] ; then
    unset LEDGER
    declare -A LEDGER=()
    sleep 70
    continue
  fi

  # read current schedule
  SCHEDULE=$(grep -v '^ *#' < ${SCHEDULE_FILE} | sed "/^ *$/d")

  # loop must not be in pipeline, because if so, it's in a subshell and LEDGER is not set globally
  while IFS= read -r JOB_SPEC ; do

    # split schedule line in hour and job
    IFS=':' read -r -a TOKEN <<< "$JOB_SPEC"
    HOUR="${TOKEN[0]}"
    MINUTE="${TOKEN[1]}"
    JOB="${TOKEN[2]}"

    if [[ \
            ${THIS_TIME} == "${HOUR}:${MINUTE}" \
         || ${HOUR} == '*' && ${THIS_MINUTE} == ${MINUTE} \
         || ${HOUR} == ${THIS_HOUR} && ${MINUTE} == '*' \
         || ${HOUR} == '*' && ${MINUTE} == '*' \
       ]] ; then
      # the job has to be executed this hour
      if [[ ${LEDGER[$JOB]} != ${THIS_TIME} ]] ; then
        # the job has not yet been executed this time
        LEDGER+=([$JOB]=${THIS_TIME})
        echo "Calling ${JOB} at $(date +'%H:%M')"
        EXECUTOR="jobs/${JOB}/job.sh"
        if [[ -r ${EXECUTOR} ]] ; then
          # execute it
          bash ${EXECUTOR}
        else
          # complain
          echo "    ${EXECUTOR} is not executable"
        fi
        echo "    Done"
      fi
    fi
    # next JOB_SPEC
  done <<< "${SCHEDULE}"

  # wait a minute before reading and processing schedule again
  sleep 10
done
