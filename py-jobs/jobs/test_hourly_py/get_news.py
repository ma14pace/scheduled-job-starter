import requests
from bs4 import BeautifulSoup


WIEN_GV_AT = "https://www.wien.gv.at"


def main():
    page = requests.get(WIEN_GV_AT)
    if page.status_code != 200:
        print("Tried to call {}, got status code {}".format(WIEN_GV_AT, page.status_code) )
        exit(1)
    soup = BeautifulSoup(page.content, 'html.parser')
    try:
        top_story_title = soup.select("div.topstory-container div h3 a")[0].get_text()
        print("Title of top story on {} is '{}'".format(WIEN_GV_AT, top_story_title))
    except:
        print("There seems to be no top story on {}".format(WIEN_GV_AT))
        exit(1)

if __name__ == "__main__":
    main()
    exit(0)
