#!/usr/bin/env bash

if [[ -f /config/CURRENT_CONFIG_VERSION ]] ; then
  CURRENT_CONFIG_VERSION=$(cat /config/CURRENT_CONFIG_VERSION)
else
  CURRENT_CONFIG_VERSION=$(cat ../CURRENT_CONFIG_VERSION)
fi

echo "    Job $(basename $(dirname $0)), current config version is ${CURRENT_CONFIG_VERSION}"

exit 0
