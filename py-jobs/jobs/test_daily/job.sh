#!/usr/bin/env bash

if [[ -f /config/CURRENT_CONFIG_VERSION ]] ; then
  CURRENT_CONFIG_VERSION=$(cat /config/CURRENT_CONFIG_VERSION)
else
  CURRENT_CONFIG_VERSION=$(cat ../CURRENT_CONFIG_VERSION)
fi

echo "    Job $(basename $(dirname $0)), current config version is ${CURRENT_CONFIG_VERSION}"
echo -n "    Python version = " ; python --version

# make sure we are in script directory
cd "$(dirname $0)"

echo "    doing some daily job in $(pwd)"

exit 0
